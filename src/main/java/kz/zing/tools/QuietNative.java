package kz.zing.tools;

public class QuietNative {
    static {
        System.loadLibrary("Quiet");
    }

    native public static int createFile(String inputFileName, String outputFileName, String profile);
}
