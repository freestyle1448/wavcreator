package kz.zing.services;

import org.springframework.scheduling.annotation.Async;

@Async
public interface MainService {
    void createWav(String transactionId, String filePath, String profile);
}
