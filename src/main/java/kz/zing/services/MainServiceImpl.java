package kz.zing.services;

import kz.zing.exceptions.NotFoundException;
import kz.zing.models.Transaction;
import kz.zing.models.User;
import kz.zing.repositories.TransactionsRepository;
import kz.zing.repositories.UsersRepository;
import kz.zing.tools.QuietNative;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Optional;

@Service
public class MainServiceImpl implements MainService {
    private final TransactionsRepository transactionsRepository;
    private final UsersRepository usersRepository;

    public MainServiceImpl(TransactionsRepository transactionsRepository, UsersRepository usersRepository) {
        this.transactionsRepository = transactionsRepository;
        this.usersRepository = usersRepository;
    }

    @Override
    public void createWav(String transactionId, String filePath, String profile) {
        final Optional<Transaction> transactionById = transactionsRepository.findById(new ObjectId(transactionId));
        if (transactionById.isEmpty())
            throw new NotFoundException("Транзакция не найдена!");
        Transaction transaction = transactionById.get();

        final Optional<User> userOptional = usersRepository.findById(transaction.getSender());
        if (userOptional.isEmpty())
            throw new NotFoundException("Отправитель не найден!");
        User user = userOptional.get();

        String textTransaction = String.format("%s:%s", transaction.getAmount().getAmount(), user.getUid());

        File dir = new File(filePath);
        if (!dir.exists()) {
            final var mkdirs = dir.mkdirs();
        }

        File text = new File("/tmp/" + transactionId + ".txt");
        boolean isCreated = false;
        try {
            isCreated = text.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (isCreated)
            try (FileWriter writer = new FileWriter(text, false)) {
                writer.write(textTransaction);
                writer.flush();
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }

        QuietNative.createFile("/tmp/" + transactionId + ".txt",
                filePath + "/" + transactionId + ".wav",
                profile);
    }
}
