package kz.zing.controllers;

import kz.zing.services.MainService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {
    private final MainService mainService;

    public MainController(MainService mainService) {
        this.mainService = mainService;
    }

    @GetMapping("/createWav")
    public void createWav(String transactionId, String path, String profile) {
        mainService.createWav(transactionId, path, profile == null ? "ultrasonic-experimental" : profile);
    }
}
