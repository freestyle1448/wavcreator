package kz.zing.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Document(collection = "transactions")
public class Transaction {
    @Id
    private ObjectId id;
    private Balance amount;
    private ObjectId Sender;
    private ObjectId Receiver;
    private String confirmationCode;
    private Boolean confirmed;
    private Date creationDate;
}
